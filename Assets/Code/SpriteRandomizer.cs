using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRandomizer : MonoBehaviour
{
    public Sprite[] diceImages;
    public int rand;

    // Start is called before the first frame update
    void Start()
    {
        rand = Random.Range(0, diceImages.Length);
        GetComponent<SpriteRenderer>().sprite = diceImages[rand];
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(rand);
    }
}
