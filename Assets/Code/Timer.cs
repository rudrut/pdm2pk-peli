using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Timer : MonoBehaviour
{
    public float initialTimeValue = 90;
    public float timeValue;
    public TMP_Text timeText;
    public bool startKeyPressed;
    public bool restartKeyPressed;
    // Start is called before the first frame update
    void Start()
    {
        timeValue = initialTimeValue;
    }

    // Update is called once per frame
    void Update()
    {
        DisplayTime(timeValue);
        RestartTimer(timeValue);
        if(Input.GetKeyDown(KeyCode.S))
        {
            startKeyPressed = true;
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            restartKeyPressed = true;
        }
        if(startKeyPressed)
        {
            StartTimer();
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        if(timeToDisplay < 0)
        {
            timeToDisplay = 0;
        }
        /*else if(timeToDisplay > 0)
        {
            timeToDisplay += 1;
        }
        */

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float milliseconds = timeToDisplay % 1 * 1000;

        timeText.text = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
    }

    void RestartTimer(float timeToDisplay)
    {
        if (restartKeyPressed && timeToDisplay <= 0)
        {
            timeValue += initialTimeValue;
            startKeyPressed = false;
        }
    }

    void StartTimer()
    {
        if (timeValue > 0 && startKeyPressed)
        {
            timeValue -= Time.deltaTime;
            restartKeyPressed = false;
        }
        else
        {
            timeValue = 0;
        }
    }
}
