using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    public Transform targetPosition;

    [SerializeField]
    [Range(0f, 1f)]
    private float lerpPct = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        //transform.position = targetPosition.position;
        //Vector3.MoveTowards(transform.position, targetPosition.position, 100f);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            transform.position = Vector2.Lerp(transform.position, targetPosition.position, lerpPct);
        }
    }
}
